$fn = 100;


height = 26;
width = 60;


module roundedRect(size, radius) 
{
	x = size[0];
	y = size[1];
	z = size[2];

	linear_extrude(height=z)
	hull()
	{
			translate([radius, radius, 0])
			circle(r=radius);
  
			translate([x-(radius), radius, 0])
			circle(r=radius);
  
			translate([radius, y-(radius), 0])
			circle(r=radius);
  
			translate([x-(radius), y-(radius), 0])
			circle(r=radius);
	}
}

module motor_holes()
{
	cylinder(10, 4, 4);
	for( i = [0:1] ) {
		rotate(45 + i*180, [0,0,1]) {
			translate([16.5/2, 0, -1]) {
				cylinder(10, 1.5, 1.5);
			}
		}
		rotate(-45 + i*180, [0,0,1]) {
			translate([13.5/2, 0, -1]) {
				cylinder(10, 1.5, 1.5);
			}
		}
	}

}

difference() {
	roundedRect([width,height,4.1], 4); 
	translate([-5, 8, -.05]) {
		cube([10,10,4.2]);
		translate([width,0,0]) {
			cube([10,10,4.2]);
		}
	}

	translate([width/2, height/2, -1]) {
		motor_holes();
	}
}


